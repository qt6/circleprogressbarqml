import QtQuick 2.15
import QtGraphicalEffects 1.12

Canvas {
    id: root
    width: Math.min(parent.width,parent.height)
    height: Math.min(parent.width,parent.height)
    antialiasing: true
    property var angle : 0
    readonly property int lineWidth :  10
    property int spots :  1

    //    property var backgroundColor : "#007cad"
//    property var backgroundColor : "#ff4500"
    property var backgroundColor : "black"
    property var foregroundColor : "white"
    //    property var foregroundColor : "#afdafc"
    //    property var foregroundColor : "red"
    //    anchors.fill: parent

    readonly property real bandwidth : 1/10



    onPaint: {

        var ctx = getContext("2d")
        var gradient = ctx.createConicalGradient(width/2,height/2,(Math.PI * angle /180))
        ctx.beginPath()

        function f(count){
            gradient.addColorStop(0, foregroundColor)
            gradient.addColorStop(bandwidth, backgroundColor)

            for(let index=1;index<count;index++){
                gradient.addColorStop(index/count-bandwidth, backgroundColor)
                gradient.addColorStop(index/count, foregroundColor)
                gradient.addColorStop(index/count+bandwidth, backgroundColor)
            }
            gradient.addColorStop(1-bandwidth, backgroundColor)
            gradient.addColorStop(1, foregroundColor)
        }


        f(spots)




        ctx.strokeStyle = gradient
        ctx.lineWidth = lineWidth
        ctx.ellipse(ctx.lineWidth/2,ctx.lineWidth/2,width-ctx.lineWidth*2,height-ctx.lineWidth*2)
        ctx.closePath()
        ctx.stroke()



    }
}






/*##^##
Designer {
    D{i:0;formeditorZoom:0.75}
}
##^##*/
